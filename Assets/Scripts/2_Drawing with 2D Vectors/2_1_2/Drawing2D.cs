﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Drawing2D : MonoBehaviour
{
    // Start is called before the first frame update
    public List<Vector2> vectors;
    private Dictionary<Vector2, GameObject> points;
    void Start()
    {
        LineRenderer lineRenderer= gameObject.GetComponent<LineRenderer>();
        List<Vector3> postions3D = new List<Vector3>();

        points = new Dictionary<Vector2, GameObject>();
        for(int i = 0; i < vectors.Count; i++){
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = new Vector3(vectors[i].x, vectors[i].y, 0);
            points.Add(vectors[i], sphere);
            postions3D.Add(sphere.transform.position);
        }
        lineRenderer.positionCount = vectors.Count;
        lineRenderer.SetPositions(postions3D.ToArray());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
